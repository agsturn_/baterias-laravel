@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Medições</h1>
        <hr>
        <h2>Resultados:</h2>
        <table class="table table-dark table-striped table-bordered" id="tbDados">
                <\<thead>
               <tr>
                <th>Pilha/bateria</th>
                <th>Tensao nominal(v)</th>
                <th>Capacidade de corrente (mA.h)</th>
                <th>Tensao sem carga(v)</th>
                <th>Tensao com carga(v)</th>
                <th>Resistência de carga(ohm)</th>
                <th>Resistencia interna(ohm)</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($medicoes as $medicao)
                    <tr>
                        <td>{{$medicao->pilha_bateria}}</td>
                        <td>{{number_format($medicao->tensao_nominal,1,'.','')}}</td>
                        <td>{{$medicao->capacidade_corrente}}</td>
                        <td>{{$medicao->tensao_sem_corrente}}</td>
                        <td>{{$medicao->tensao_com_corrente}}</td>
                        <td>{{$medicao->resistencia_carga}}</td>
                        <td>{{number_format($medicao->resistencia_interna,3,'.','')}}</td>
                    </tr>    
                    @endforeach
                </tbody>
        </table>
    </main>
    @endsection

    @section('rodape')
    
    <h4>Rodape da pagina principal</h4>
    
    @endsection

