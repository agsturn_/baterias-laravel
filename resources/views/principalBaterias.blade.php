@extends('templates.base')

@section('conteudo')
    <main>

        <h1>turma: 2D2 - Grupo 1</h1>
        <h2>participantes</h2>
        <hr>
        <table class="table table-stripedd table-bordered">
            <tr>
                <th>matricula</th>
                <th>nome</th>
                <th>função</th>
            </tr>  
            <tr>
                <td>0073587</td>
                <td>Ana Gabriela</td>
                <td>gerente</td>
            </tr> 
            <tr>
                <td>0072531</td>
                <td>Ana julia</td>
                <td>Medidor</td>
            </tr> 
            <tr>
                <td>0073004</td>
                <td>Camila Bicalho</td>
                <td>Medidor</td>
            </tr> 
            <tr>
                <td>0066682</td>
                <td>Iara Geralda</td>
                <td>Medidor</td>
            </tr> 
            <tr>
                <td>0072541</td>
                <td>Nicolas Aniceto</td>
                <td>Programação</td>
            </tr>
        </table>
        
        <img class="grupo"src="../imgs/grupo.jpeg"  alt="../bateria"> 
    </main>
    
 @endsection

@section('rodape')
<h4>Rodape da pagina principal</h4>
@endsection
