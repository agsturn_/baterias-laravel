//alert('ok');

//  objeto JSON com os dados
var dados = [
    {
      pilha: "Pilha Alcalina Duracel AA",
      tensao_nominal: 1.5,
      corrente: 2800,
      E: 1.304,
      V: 1.286,
      R: 24.1,
    },
    {
      pilha: "Bateria Unipower",
      tensao_nominal: 12,
      corrente: 7000,
      E: 10.49,
      V: 10.26,
      R: 24.1,
    },
    {
      pilha: "Pilha duracel AAA",
      tensao_nominal: 1.5,
      corrente: 900,
      E: 0.987,
      V:  0.866,
      R: 24.1,
    },
    {
      pilha: "Philips AAA",
      tensao_nominal: 1.5,
      corrente: 1200,
      E:1.371, 
      V:  1.312,
      R: 24.1,
    },
    {
      pilha: "Luatek",
      tensao_nominal: 3.7,
      corrente: 1200,
      E:2.516, 
      V:2.515,
      R: 24.1,
    },
    {
      pilha: "Energy Elgin",
      tensao_nominal:9,
      corrente: 250,
      E:7.21, 
      V:2.211,
      R: 24.1,
    },
    {
      pilha: "Golite",
      tensao_nominal: 9,
      corrente: 500,
      E:5.90, 
      V:0.27766,
      R: 24.1,
    },
    {
      pilha: "JYX",
      tensao_nominal:3.7-4.2,
      corrente: 9800,
      E:2.466, 
      V:2.266,
      R: 24.1,
    },
    {
      pilha: "Panasonic AA",
      tensao_nominal: 1.5,
      corrente: 2550,
      E:1.379, 
      V:  1.329,
      R: 24.1,
    },
    {
      pilha: "Freedom",
      tensao_nominal: 12,
      corrente: 30000,
      E:10.68, 
      V:10.63,
      R: 24.1,
    },
  ];
  
  // Função para calcular a resistencia interna de cada medição e preencher a tabela
  function calcularResistencia() {
    var tabela = document.getElementById("tbDados");
  
    // Limpar tabela antes de preencher
    var tabelaHTML = `<tr>
                              <th>Pilha/Bateria</th>
                              <th>Tensão nominal (V)</th>
                              <th>Capacidade de corrente (mA.h)</th>
                              <th>Tensão sem carga(V)</th>
                              <th>Tensão com carga(V)</th>
                              <th>Resitência de carga(ohm)</th>
                              <th>Resistência interna(ohm)</th>
                    </tr>`;
  
    // Iterar sobre os dados e calcular o r de cada um
    for (var i = 0; i < dados.length; i++) {
      var linha = dados[i];
      var E = linha.E;
      var V = linha.V;
      var R = linha.R;
      var r = R * (E / V - 1);
  
      // Adicionar nova linha à tabela com os valores e a soma
      var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
  
      tabelaHTML += novaLinha;
    }
    tabela.innerHTML = tabelaHTML;
  }
  
  calcularResistencia();

